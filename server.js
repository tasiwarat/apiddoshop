const express = require('express');
const bodyParser = require('body-parser');

var mysql = require("mysql");

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "ddoshopdb"
});
con.connect(function (err) {
    if (err) {
        console.log('Error connecting to Db');
        return;
    }
    console.log('Connection established');
});

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/', (req, res) => {
	var db = con
});

app.post('/product', (req, res) => {
    if (req.body) {

        let _name = req.body.name;
        console.log(_name)
        var db = con
        var data = "SELECT * FROM tbproduct WHERE name like '%"+_name+"%'";
        console.log(data);
        db.query(data,function(err,rows){
            console.log(rows);
            var data = rows;
            res.status(200).send({data: data });
        });
    } else {
		res.status(404).send({data: "Can't found data in the body." });
    }

});


app.get('/login', (req, res) => {
    let user = req.query.user;
    let pass = req.query.pass;
	var db = con
    var data = "SELECT * FROM tbuser WHERE Id = '"+user+"' and Password = '"+pass+"'";
    console.log(data);
	db.query(data,function(err,rows){
		console.log(rows);
        if (rows.length > 0) {
            var data = rows;
            res.status(200).send({data: data });
        } else {
            res.status(404).send({message: "Can't found user" })
        }
	});
});

app.get('/getuser', (req, res) => {
    // let user = req.query.user;
    // let pass = req.query.pass;
	var db = con
    var data = "SELECT * FROM tbuser";
    console.log(data);
	db.query(data,function(err,rows){
		console.log(rows);
        if (rows.length > 0) {
            var data = rows;
            res.status(200).send({data: data });
        } else {
            res.status(404).send({message: "Can't found user" })
        }
	});
});

app.post('/login', (req, res) => {
    
});

app.post('/Booking', (req, res) => {
    

    var dataReq = JSON.parse(req.body.dataBooking);
    var dataUser = req.body.user;
    var dataPrice = req.body.price;
    var _RefBooking = "REFP"+new Date().getTime()
	var db = con;
    
    let getTime = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
    var data = 'INSERT INTO `tbtransection`(`IdUser`, `RefId`, `IdProduct`, `Quality`, `UpdateAt`) '+
    'VALUES ';
    dataReq.forEach((resBooking, index) => {
        if (index == 0) {
            data += '("';
        } else {
            data += ',("';
        }
        data += dataUser+'","'+
        _RefBooking+'","'+
        resBooking.product_id+'",'+
        resBooking.quality+',"'+
        getTime+'")';
    });
    console.log(data);
    db.query(data, function (err, result) {
        if (err) {
            res.status(400).send(err)
        } else {

            let sqlInseart = 'INSERT INTO `tbbooking`(`Id`, `RefId`, `Price`, `Status`, `CreateDate`) VALUES '+
            '("'+
            dataUser
            +'","'+
            _RefBooking
            +'","'+
            dataPrice
            +'",0,"'+
            getTime
            +'")'

            db.query(sqlInseart, function (err, result) {
                if (err) {
                    res.status(400).send(err)
                } else {
                    res.status(200).send(result)
                }
            });

        }
	});


})

app.post('/Register', (req, res) => {
    let user = req.query.user;
    let pass = req.query.pass;
    let Email = req.query.Email;
    let Updateat = new Date().toISOString();
	var db = con;

    var data = "INSERT INTO tbuser(`Id`, `Password`, `Email`, `Status`, `UpdateAt`) VALUES ('"+
    user+"','"+
    pass+"','"+
    Email+"',1,'"+Updateat+"')";
    console.log(data);
	db.query(data,function(err,rows){
		console.log(rows);
        if (rows) {
            var data = rows;
            res.status(200).send({data: {
                User:user,
                Pass: pass,
                Email: Email,
                Status: 1,
                UpdateAt: Updateat
            } });
        } else             res.status(404).send({message: "Can't regiser user" })
{
        }
	});
});


app.get('/TransectionAll', (req, res) => {
    let _user = req.query.user;
    let sql = 'SELECT * FROM `tbbooking`';
	var db = con;

	db.query(sql,function(err,rows){
		console.log(rows);
        if (rows.length > 0) {
            res.status(200).send({data: rows});
        } else {
            var data = rows;
            res.status(200).send({data: []});
        }
	});
})

app.get('/Transection', (req, res) => {
    let _user = req.query.user;
    let sql = 'SELECT * FROM `tbbooking` WHERE Id = "'+_user+'"';
	var db = con;

	db.query(sql,function(err,rows){
		console.log(rows);
        if (rows.length > 0) {
            res.status(200).send({data: rows});
        } else {
            var data = rows;
            res.status(200).send({data: []});
        }
	});
})


app.get('/Transection/Detail', (req, res) => {
    let _user = req.query.user;
    let sql = 'SELECT * FROM tbtransection WHERE IdUser = "'+_user+'"';
	var db = con;

	db.query(sql,function(err,rows){
		console.log(rows);
            var data = rows;
            res.status(200).send({data: data});
	});
})

app.get('/TransectionDetail', (req, res) => {
    let _Ref = req.query.Ref;
    let sql = 'SELECT tbproduct.name, tbproduct.image, tbproduct.description'+
    ', tbproduct.quality, tbproduct.price, tbproduct.price* tbproduct.quality AS "summary"'+
    ' FROM tbtransection INNER JOIN tbproduct ON tbtransection.IdProduct = tbproduct.index'+
    ' WHERE tbtransection.RefId = "'+_Ref+'"';
    console.log(sql);
	var db = con;

	db.query(sql,function(err,rows){
		console.log(rows);
            var data = rows;
            res.status(200).send({data: data});
	});
})



// Listen to the App Engine-specified port, or 8080 otherwise
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}...`);
});

